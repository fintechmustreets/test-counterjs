import React from 'react'
import ReactDOM from 'react-dom'
import Provider from "redhooks"
import ToDo from './Slomux'
import * as serviceWorker from './serviceWorker'
import {store} from './Slomux/Store'


ReactDOM.render(
  <Provider store={store}>
    <ToDo title="Список задач" />
  </Provider>,
  document.getElementById('root')
)

serviceWorker.unregister()
