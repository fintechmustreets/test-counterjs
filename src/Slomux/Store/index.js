import { createStore } from "redhooks"

const counter = (state = 0, { type, payload }) => {
    switch (type) {
        case "INCREMENT":
            return state + 1
        case "DECREMENT":
            return state - 1
        case "INCREMENTVALUE":
            return state + payload
        case "DECREMENTVALUE":
            return state - payload
        case "STOP":
            return payload
        default:
            return state
    }
}

const store = createStore(counter)

export { store }