// const Timer = connect(state => ({
//     currentInterval: state,
//   }), () => {})(TimerComponent)

import React, { useState, useEffect } from 'react'
import { connect } from 'redhooks'
import PropTypes from 'prop-types'
import IntervalComponent from './Interval'
import FormCounter from './Form'
import styled from 'styled-components'


const mapDispatchToProps = dispatch => ({
    counterReducer: action =>
        dispatch({
            type: action.type,
            payload: action.payload,
        }),
})

const mapStateToProp = state => ({
    counter: state,
})

const TimerComponent = (props) => {
    const { counter, counterReducer } = props
    const [currentTime, setState] = useState(null)

    useEffect(() => {
        const timer = setInterval(() => {
            if (counter < 0) {
                return setState(currentTime + counter)
            }
            return setState(currentTime + counter)
        }, counter < 0 ? -counter : counter)

        return () => {
            clearInterval(timer)
        }

    }, [currentTime, counter])


    return (
        <Container>
            <Interval />
            <div>
                Секундомер: {currentTime} сек.
          </div>
            <SpanContainer>
                <ButtonContainer onClick={() => { setState(counter) }}>Старт</ButtonContainer>
                <ButtonContainer onClick={() => { counterReducer({ type: 'STOP', payload: 0 }) }}>Стоп</ButtonContainer>
                <ButtonContainer onClick={() => { setState(0) }}>Сбросить</ButtonContainer>
            </SpanContainer>
            <FormCounter />
        </Container>
    )
}

const Interval =styled(IntervalComponent)`
    display:flex;
    justify-content:center;
    justify-self:center;
    justify-items:center;
    flex-direction:column;
    align-items:center;
`

const Container = styled.div`
    display:flex;
    width:356px;
    height:306px;
    justify-content:center;
    justify-self:center;
    justify-items:center;
    flex-direction:column;
    align-items:center;
`


const SpanContainer=styled.span`
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    width: 330px;
    margin: 0 auto;
`
const ButtonContainer = styled.button`
    border-radius: 25px;
    border-width:1px;
    border-color:#000;
    width: 70px;
    margin: 5px;
    height: 30px;
    background-color: #242424;
    color: white;
    font-size: 12px;
`


TimerComponent.propType = {
    counter: PropTypes.object,
    counterReducer: PropTypes.object
}

export default connect(mapStateToProp, mapDispatchToProps)(TimerComponent)
