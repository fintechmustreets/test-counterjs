import React, { useState } from 'react'
import { connect } from 'redhooks'
import PropTypes from 'prop-types'
import validator from 'validator'
import styled from 'styled-components'

const mapDispatchToProps = dispatch => ({
    counterReducer: action =>
        dispatch({
            type: action.type,
            payload: action.payload,
        }),
})

const FormCounter = (props) => {
    const { counterReducer } = props
    const [change, setChange] = useState('')
    const [error, setError] = useState('')


    const handleSubmit = (event) => {
        event.preventDefault()
        console.log(validator.isAlphanumeric(change))
        if (validator.isNumeric(change)) {
            setError({ state: '' })
            return counterReducer({ type: 'STOP', payload: parseInt(change) })
        }
        return setError({ state: 'Необходимо ввести цифры:' })

    }

    const handleChange = (e) => {
        setChange(e.target.value)
    }

    return (
        <FormData style={{ 'display': ['flex', '-webkit-flex'], marginTop: '10px' }} onSubmit={(event) => handleSubmit(event)}>
            <Title>Ручное Управление Счетчиком:</Title>
            <label>{error.state}</label>
            <InputValue type="text" name="name" onChange={(e) => handleChange(e)} />
            <SubmitButton type="submit" value="Применить" />
        </FormData>
    )
}

const Title= styled.label`
    font-size:18px;
    font-weight:bold;
    color: #ffff;
`
const InputValue= styled.input`
    font-size:14px;
    font-weight:400;
    color: #000;
    padding:4px;
    text-align:center;
`


const SubmitButton = styled.input`
    background-color:#ffff;
    border-radius:25px;
    padding: 5px; /* Поля */
    font-size: 14px; /* Размер текста */
    color: #000;
    justify-content:center;
    align-items:center;
`


const FormData = styled.form`
    border-radius:25px;
    border-width:2px;
    border-color:#000;
    background-color:#bdbdbd;
    padding:15px;
    height: 55px; /* Высота */
    display:flex;
    padding:14px;
    justify-content:space-between;
    flex-direction:column;
    flex:1;
`

FormCounter.propType = {
    counter: PropTypes.object,
    counterReducer: PropTypes.object
}

export default connect(null, mapDispatchToProps)(FormCounter)