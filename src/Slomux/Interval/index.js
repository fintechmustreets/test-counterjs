
import React from 'react'
import { connect } from 'redhooks'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const mapDispatchToProps = dispatch => ({
    counterReducer: action =>
        dispatch({
            type: action.type,
        }),
})

const mapStateToProp = state => ({
    counter: state,
})

const IntervalComponent = (props) => {
    const { counter, counterReducer } = props
    const number = (counter, key) => {
        if (counter !== 0) {
            if (key) return counter - 1
            return counter + 1
        }
        if (key) return '-1'
        return '+1'
    }
    const SpanButtom = () => {
        return (
            <Container>
                <Title>Интервал обновления секундомера:</Title>
                <Title>{counter} сек.</Title>
                <SpanContainer>
                    <ButtonContainer onClick={() => counterReducer({ type: "DECREMENT" })}>{number(counter, true)} </ButtonContainer>
                    <ButtonContainer onClick={() => counterReducer({ type: "INCREMENT" })}>{number(counter)} </ButtonContainer>
                </SpanContainer>
            </Container>
        )
    }
    return SpanButtom()
}

IntervalComponent.propTypes = {
    counter: PropTypes.number,
    counterReducer: PropTypes.func
}

const Title = styled.h1`
    font-size:20px;
    color: #000;
    line-height:10%;
    font-weight:bold;
    text-align:center;
`

const SpanContainer = styled.span`
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    width: 330px;
    margin: 0 auto;
`
const ButtonContainer = styled.button`
    border: none;
    width: 70px;
    margin: 5px;
    height: 30px;
    background-color: green;
    color: white;
    font-size: 16px;
`

const Container = styled.div`
    display:flex;
    justify-content:center;
    justify-self:center;
    justify-items:center;
    flex-direction:column;
    align-items:center;
`

export default connect(mapStateToProp, mapDispatchToProps)(IntervalComponent)